package rasyidk.fa.rotikuadmin.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import rasyidk.fa.rotikuadmin.MainActivity;
import rasyidk.fa.rotikuadmin.R;
import rasyidk.fa.rotikuadmin.adapter.ProdukAdapter;
import rasyidk.fa.rotikuadmin.model.RotiItem;

public class ProdukFragment extends Fragment {

    DatabaseReference databaseReference;
    private ArrayList<RotiItem> listRoti = new ArrayList<>();
    private RecyclerView rv_roti;

    public ProdukFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_produks, container, false);
        rv_roti = view.findViewById(R.id.rv_roti);

//        final ActionBar actionBar = getSupportActionBar();
//        getActionBar().setDisplayHomeAsUpEnabled(true);
       // Toolbar topToolbar = view.findViewById(R.id.add_produk);
        setHasOptionsMenu(true);

        databaseReference = MainActivity.getDatabase().getReference();
        databaseReference.keepSynced(true);

        databaseReference.child("products").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    RotiItem item = snapshot.getValue(RotiItem.class);
                    item.setKey(snapshot.getKey());

                    listRoti.add(item);
                }
                StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL);
                rv_roti.setLayoutManager(staggeredGridLayoutManager);
                rv_roti.setAdapter(new ProdukAdapter(getContext(), listRoti));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        return view;
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.barmenu, menu);

        //super.onCreateOptionsMenu(menu);
    }
    

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_produk:
                Toast.makeText(getContext(), "add", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }
}
